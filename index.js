const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

const app = express()
const port = 4000

dotenv.config()

// set up mongoose connection
mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.rgocvci.mongodb.net/?retryWrites=true&w=majority`,
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
)

// assign mongoose connection to a variable

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', ()=>console.log('Connected to MongoDB!'))


// middleware
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})


// model
const User = mongoose.model('User', userSchema)


// Route
app.post('/signup', (request,response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate user found!")
		} else {
			if(request.body.username != '' && request.body.password != ''){

				let newUser = new User ({
					username: request.body.username,
					password: request.body.password
				})

				newUser.save((error, result)=>{
					if(error){
						return response.send(error)
					}

					return response.send('New User registered')
				})
			} else {
				return response.send('BOTH Username AND Password must be provided.')
			}
		}
	})
})



app.listen(port, () => console.log(`Server is running at port: ${port}`))